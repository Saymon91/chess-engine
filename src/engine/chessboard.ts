import type { Piece } from './index';
import type { ColumnNames, PieceColor, RowNames } from './types';
import { Square } from './square';

export class Chessboard {
  public pieces: Record<PieceColor, Piece[]> = {
    white: [],
    black: [],
  };

  public squares: Partial<
    Record<RowNames, Partial<Record<ColumnNames, Square>>>
  > = {};

  private init() {
    for (const color in this.pieces) {
      this.pieces[color].push(...Array(8));
    }
  }

  getSquare(rowName, colName): Square | undefined {
    if (rowName < 0 || rowName > 7 || colName < 0 || colName > 7) {
      return undefined;
    }
    return this.squares[rowName]?.[colName] ?? new Square();
  }
}
