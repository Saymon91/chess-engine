import { AbstractPiece } from './abstract-piece';
import type { SquareLocation } from './types';

export class Rook extends AbstractPiece {
  public readonly type = 'rook';

  public getAllowedMoves(): SquareLocation[] {
    return [];
  }
}
