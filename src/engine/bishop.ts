import { AbstractPiece } from './abstract-piece';
import type { SquareLocation } from './types';

export class Bishop extends AbstractPiece {
  public readonly type = 'bishop';

  public getAllowedMoves(): SquareLocation[] {
    return [];
  }
}
