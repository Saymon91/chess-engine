import { AbstractPiece } from './abstract-piece';
import { SquareLocation } from './types';

export class Queen extends AbstractPiece {
  public readonly type = 'queen';

  public getAllowedMoves(): SquareLocation[] {
    return [];
  }
}
