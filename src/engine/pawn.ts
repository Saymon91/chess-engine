import { AbstractPiece } from './abstract-piece';
import type { ColumnNames, PieceType, RowNames, SquareLocation } from './types';
import { Queen } from './queen';
import { Bishop } from './bishop';
import { Rook } from './rook';
import { Knight } from './knight';

export class Pawn extends AbstractPiece {
  public readonly type = 'pawn';

  public get longMove(): boolean {
    return this.isLongMove;
  }

  private getNextY(y: number): number {
    return this.color === 'white' ? y + 1 : y - 1;
  }

  public getAllowedMoves(): SquareLocation[] {
    const moves: SquareLocation[] = [];
    const [x, y] = this.currentLocation;
    const nextY = this.getNextY(y);
    let nextSquare = this.chessboard.getSquare(x, nextY);

    if (nextSquare && !nextSquare.piece) {
      moves.push([x, nextY as RowNames]);
    }

    // Проход пешки возможен только если аешка еще не ходила и перед ней свободны 2 клетки.
    if (moves.length && !this.moves) {
      const nextYLongMove = this.getNextY(nextY);
      nextSquare = this.chessboard.getSquare(x, nextYLongMove);
      if (nextSquare && !nextSquare.piece) {
        moves.push([x, nextYLongMove as RowNames]);
      }
    }

    // Проверка фигур, который пешка может побить
    [
      [x - 1, nextY],
      [x + 1, nextY],
    ].forEach(([nextX, nextY]) => {
      nextSquare = this.chessboard.getSquare(nextX, nextY);
      if (
        nextSquare &&
        nextSquare.piece &&
        nextSquare.piece.color !== this.color
      ) {
        moves.push([nextX as ColumnNames, nextY as RowNames]);
      }
    });

    // Проверка возможности взятия на проходе
    [
      [x - 1, y],
      [x + 1, y],
    ].forEach(([x, y]) => {
      const square = this.chessboard.getSquare(x, y);

      if (
        square &&
        square.piece &&
        square.piece.color !== this.color &&
        square.piece.type === 'pawn' &&
        (square.piece as Pawn).isLongMove
      ) {
        moves.push([x as ColumnNames, nextY as RowNames]);
      }
    });

    return moves;
  }

  private isLongMove = false;

  private upgrade(
    type: Extract<PieceType, 'queen' | 'bishop' | 'rook' | 'knight'>,
  ): Queen | Bishop | Rook | Knight {
    this.chessboard.getSquare(...this.currentLocation).unsetPiece();
    switch (type) {
      case 'queen':
        return new Queen(this.color, this.currentLocation, this.chessboard);
      case 'bishop':
        return new Bishop(this.color, this.currentLocation, this.chessboard);
      case 'rook':
        return new Rook(this.color, this.currentLocation, this.chessboard);
      case 'knight':
        return new Rook(this.color, this.currentLocation, this.chessboard);
      default:
        throw new Error(`The pawn cannot be upgraded to "${type}"`);
    }
  }
}
