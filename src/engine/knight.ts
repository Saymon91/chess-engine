import { AbstractPiece } from './abstract-piece';
import type { SquareLocation } from './types';

export class Knight extends AbstractPiece {
  public readonly type = 'knight';

  public getAllowedMoves(): SquareLocation[] {
    return [];
  }
}
