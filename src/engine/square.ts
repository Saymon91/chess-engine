import { AbstractPiece } from './abstract-piece';

export class Square {
  public get piece(): AbstractPiece | null {
    return this.locatedPiece;
  }

  constructor() {}

  public unsetPiece(): Square {
    this.locatedPiece = null;

    return this;
  }

  public setPiece(piece: AbstractPiece): Square {
    if (this.locatedPiece) {
      if (this.locatedPiece.color === piece.color) {
        throw new Error('Piece cannot be moved here');
      }

      this.locatedPiece.kill(piece);
    }
    this.locatedPiece = piece;

    return this;
  }

  private locatedPiece: AbstractPiece | null = null;
}
