const colNames = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
const getColName = (colIdx: number): string => colNames[colIdx];
const getRowName = (rowIdx: number) => rowIdx + 1;

export type Piece = King | Queen | Rook | Bishop | Knight;
