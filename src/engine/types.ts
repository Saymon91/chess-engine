import { Piece } from './index';

export type RowNames = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;
export type ColumnNames = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;

export type SquareLocation = [ColumnNames, RowNames];

export type PieceColor = 'black' | 'white';
export type PieceType =
  | 'king'
  | 'queen'
  | 'bishop'
  | 'knight'
  | 'rook'
  | 'pawn';

export interface PieceCommon {
  readonly color: PieceColor;
  readonly live: boolean;
  readonly location: SquareLocation | null;

  kill(by: Piece): this;
  move(location: SquareLocation): this;
  getAllowedMoves(): SquareLocation[];
}
