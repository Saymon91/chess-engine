import { Piece } from './index';
import { AbstractPiece } from './abstract-piece';
import type { PieceCommon, SquareLocation } from './types';

export class King extends AbstractPiece implements PieceCommon {
  public readonly type = 'king';

  public kill(by: Piece): this {
    throw new Error('The king cannot day');
  }

  public getAllowedMoves(): SquareLocation[] {
    return [];
  }

  move(location: SquareLocation): this {
    if (!this.getAllowedMoves().includes(location)) {
      throw new Error(`The ${this.type} cannot move here`);
    }

    return super.move(location);
  }
}
