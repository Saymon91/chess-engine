import { Chessboard } from './chessboard';
import type {
  PieceColor,
  PieceCommon,
  PieceType,
  SquareLocation,
} from './types';

export abstract class AbstractPiece implements PieceCommon {
  public abstract readonly type: PieceType;
  protected moves = 0;

  public get live() {
    return this.isLive;
  }
  public get location() {
    return this.currentLocation;
  }

  constructor(
    public readonly color: PieceColor,
    protected currentLocation: SquareLocation,
    protected readonly chessboard: Chessboard,
    protected isLive = true,
  ) {
    if (this.isLive) {
      this.chessboard.getSquare(...this.currentLocation).setPiece(this);
    }
  }

  public kill(by: AbstractPiece): this {
    this.isLive = false;
    this.chessboard.getSquare(...this.location).unsetPiece();
    this.currentLocation = null;

    console.warn(
      `The ${this.color} ${this.type} has been killed by ${by.color} ${by.type}`,
    );

    return this;
  }

  public move(location: SquareLocation): this {
    this.chessboard.getSquare(...this.location).unsetPiece();
    this.chessboard.getSquare(...location).setPiece(this);
    this.currentLocation = location;
    this.moves++;

    console.info(
      `The ${this.color} ${this.type} has been moved to ${this.location.join(
        ',',
      )} square`,
    );

    return this;
  }

  public abstract getAllowedMoves(): SquareLocation[];
}
